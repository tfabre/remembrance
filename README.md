# Remembrance
Remembrance is a kind of TODO LIST++. It allows to create a task hierarchy to organise and administrate a project.

## Technology
Remembrance is a web application coded with MongoDB, Express, AngularJs and Node.js.
