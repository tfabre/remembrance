var express = require("express");
var app = express();

var MongoClient = require("mongodb").MongoClient;
var morgan = require("morgan");

var conf = require("./conf");

app.use(morgan(conf.logger.format));

MongoClient.connect(conf.db.dsn, function(err, db)
{
    if (err)
        throw err;

    var router = require("./routes")(app, db);
});

app.listen(22000);
console.log("Server running on localhost:22000");
