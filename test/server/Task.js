var MongoClient = require("mongodb").MongoClient;
var should = require("should");
var Task = require("../../models/Task");

var mongoUrl = "mongodb://localhost:27017/remembrance";

describe("The Task class", function()
{
    var db;
    var task;
    var tasksCount;
    var taskToFound;

    // init
    before(function(done)
    {
        MongoClient.connect(mongoUrl, function(err, database)
        {
            if (err)
                throw err;

            db = database;
            task = new Task(db);

            db.collection("tasks").count({}, function(err, totalOfTasks)
            {
                tasksCount = totalOfTasks;
                done();
            });
        });
    });

    it("should return all the task in an array", function(done)
    {
        task.getAll(function(tasks)
        {
            tasks.should.be.an.instanceOf(Array).
                and.have.a.lengthOf(tasksCount);
            done();
        });
    });

    before(function(done)
    {
        db.collection("tasks").findOne(function(err, theTaskToFound)
        {
            if (err)
                throw err;
            taskToFound = theTaskToFound;
            done();
        });
    });

    it("should return a task by id", function(done)
    {
        var taskId = taskToFound._id.toString();
        task.get(taskId, function(theTask)
        {
            theTask.should.be.eql(taskToFound);
            done();
        });
    });
});
