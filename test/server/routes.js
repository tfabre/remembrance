var MongoClient = require("mongodb").MongoClient;
var request = require("supertest");
var should = require("should");

var mongoUrl = "mongodb://localhost:27017/remembrance"

describe("Routes for", function()
{
    var url = "localhost:22000";

    describe("GET", function()
    {
        describe("/task/", function()
        {
            var tasksCount;

            before(function(done)
            {
                MongoClient.connect(mongoUrl, function(err, db)
                {
                    if (err)
                        throw err;

                    db.collection("tasks").
                        count({}, function(err, totalOfTasks)
                    {
                        tasksCount = totalOfTasks;
                        db.close();
                        done();
                    });
                });
            });

            it("should return the list of all tasks", function(done)
            {
                request(url).
                    get("/task").
                    expect(200).
                    expect("Content-Type", "application/json; charset=utf-8").
                    end(function(err, res)
                    {
                        if (err)
                            throw err;

                        res.body.should.be.an.instanceOf(Array).
                            and.have.lengthOf(tasksCount);
                        done();
                    });
            });

            it("should respect the task schema", function(done)
            {
                var expectedKeys = ["_id", "name", "description"];

                request(url).
                    get("/task").
                    expect(200).
                    expect("Content-Type", "application/json; " +
                           "charset=utf-8").
                    end(function(err, res)
                    {
                        if (err)
                            throw err;

                        res.body.forEach(function(task)
                        {
                            task.should.have.keys(expectedKeys);
                        });

                        done();
                    });
            });
        });

        describe("/task/:id", function()
        {
            var task;

            before(function(done)
            {
                MongoClient.connect(mongoUrl, function(err, db)
                {
                    if (err)
                        throw err;

                    db.collection("tasks").
                        findOne({}, {}, function(err, taskFound)
                    {
                        task = taskFound;

                        db.close();
                        done();
                    });
                });
            });

            it("should return the corresponding task", function(done)
            {
                request(url).
                    get("/task/" + task._id).
                    expect(200).
                    expect("Content-Type", "application/json; " +
                           "charset=utf-8").
                    end(function(err, res)
                    {
                        if (err)
                            throw err;

                        task._id = task._id.toString();
                        res.body.should.be.eql(task);
                        
                        done();
                    });
            });

            it("should return a 404 error when the task is not found",
               function(done)
            {
               request(url).
                    get("/task/not_exist").
                    expect(302).
                    expect("Location", "/task/not_found").
                    end(function(err, res)
                    {
                        if (err)
                            throw err;

                        request(url).
                            get(res.res.headers.location).
                            expect(404).
                            expect("Content-Type", "text/html; charset=utf-8",
                                   done);
                    });
            });
        });
    });
});
