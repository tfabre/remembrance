var router = require("express").Router();
var Task = require("../models/Task");

module.exports = function(db)
{
    var taskRequester = new Task(db);

    router.get("/task/", function(req, res)
    {
        taskRequester.getAll(function(tasks)
        {
            res.send(tasks);
        });
    });
    
    router.get("/task/not_found", function(req, res, next)
    {
        res.status(404);
        res.send("<!DOCTYPE html>\n" +
                 "<html>\n" +
                 "  <body>\n" +
                 "    <h1>404 <small>Task not found</small></h1>\n" +
                 "  </body>\n" +
                 "</html>");
    });

    router.param("id", function(req, res, next, id)
    {
        if (!Task.isValidId(id))
            return res.redirect("/task/not_found");
        else
            next();
    });

    router.get("/task/:id", function(req, res, next)
    {
        var taskId = req.params.id;

        taskRequester.get(taskId, function(task)
        {
            if (!task)
                return res.redirect("/task/not_found");

            res.send(task);
        });
    });

    return router;
};
