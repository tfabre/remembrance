module.exports = {
    "db": {
        "dsn": "mongodb://localhost:27017/remembrance"
    },

    "logger": {
        "format": "dev"
    }
}
