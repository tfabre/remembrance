#! /bin/bash

SRC_DIR=$(dirname $0)
TASKS_DUMP_FILE=$SRC_DIR/dump/tasks.json
DATABASE=remembrance
COLLECTION=tasks

function helpMsg
{
    echo "Use: db [import] | [export]"
    echo "Export database into files or import the database from these files"
    echo
    echo "Choose one:"
    echo "  db import        To import the dataset"
    echo "  db export        To export the dataset"
}

case $1 in
    "import")
        mongoimport -d $DATABASE -c $COLLECTION --jsonArray < $TASKS_DUMP_FILE
        ;;

    "export")
        mongoexport -d $DATABASE -c $COLLECTION --jsonArray -o $TASKS_DUMP_FILE
        ;;

    *)
        helpMsg
        ;;
esac

