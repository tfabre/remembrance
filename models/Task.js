var ObjectId = require("mongodb").ObjectId;

/**
 * Allows to get or handle tasks from the database.
 *
 * @class Represents an interface between database and Tasks.
 * @param {Object} db A database object to work with.
 * @return {Function} Constructor for Task type.
 */
function Task(db)
{
    this.tasks = db.collection("tasks");
}

/**
 * Get all the Task from database and give them to the callback parameter in
 * form of Array.
 * 
 * @param {Function} callback The function to handle the Array of all tasks
 *        found. This function have just one parameter which will contain the
 *        task array.
 */
Task.prototype.getAll = function(callback)
{
    this.tasks.find().toArray(function(err, allTasks)
    {
        if (err)
            throw err;

        callback(allTasks);
    });
};

/**
 * Get a Task from database with the id given in paramater, and send the found
 * task to the callback.
 *
 * @param {String} id The task id to look for in the database.
 * @param {Function} callback The function to handle the task found. This
 *        function have just one parameter which will contain the found task.
 */
Task.prototype.get = function(id, callback)
{
    var query = {_id: new ObjectId(id)};
    this.tasks.findOne(query, function(err, task)
    {
        if (err)
            throw err;

        callback(task);
    });
};

/**
 * Return if the id given in paramter is a valid id or not.
 *
 * @static
 * @param {String} id The id to check
 * @return {Boolean} Return true if the parameter is a valid id, false otherwise
 */
Task.isValidId = function(id)
{
    return ObjectId.isValid(id);
};

module.exports = Task;
